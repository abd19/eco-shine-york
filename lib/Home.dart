import 'dart:ui';

import 'package:cupertino_stepper/cupertino_stepper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:fluttercatalog/Screens/ProfileScreen.dart';
import 'package:fluttercatalog/utils/styles.dart';
import 'package:im_stepper/stepper.dart';

import 'CustomWidget/CustomAppbar.dart';
import 'CustomWidget/CustomListView.dart';
import 'Login/signup3.dart';

class Home extends StatefulWidget {
  const Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<CustomListView> customListView = [
    new CustomListView(),
    new CustomListView(),
    new CustomListView(),
    new CustomListView(),
    new CustomListView(),
  ];
  List<Map<String, String>> ListviewData = [
    {
      "title": "VEHICLE TYPE",
    },
    {
      "title": "WASH PACKAGES",
    },
    {
      "title": "SERVICES MENU",
    },
    {
      "title": "SELECT DATE AND TIME",
    },
    {
      "title": "BOOKING SUMMARY",
    },
  ];
  int _currentStep = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Drawer(
            child: ListView(
          // Important: Remove any padding from the ListView.
          children: <Widget>[
            DrawerHeader(
              child: Text(
                'Welcome Abd',
                style: TextStyle(
                    fontFamily: "ink free",
                    fontSize: MediaQuery.of(context).size.width * 0.06,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    decoration: TextDecoration.none),
              ),
              decoration: BoxDecoration(
                color: Color(0xff96C6D9),
              ),
            ),
            ListTile(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  IconButton(
                      icon: Icon(
                    Icons.home,
                    color: Colors.black,
                  )),
                  Text(
                    "Home",
                    style: TextStyle(
                        fontFamily: "ink free",
                        fontSize: MediaQuery.of(context).size.width * 0.06,
                        fontWeight: FontWeight.w300,
                        color: Colors.black,
                        decoration: TextDecoration.none),
                  )
                ],
              ),
              onTap: () {
                // Update the state of the app.
                // ...
              },
            ),
            ListTile(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  IconButton(
                      icon: Icon(
                    Icons.person,
                    color: Colors.black,
                  )),
                  Text(
                    "Profile",
                    style: TextStyle(
                        fontFamily: "ink free",
                        fontSize: MediaQuery.of(context).size.width * 0.06,
                        fontWeight: FontWeight.w300,
                        color: Colors.black,
                        decoration: TextDecoration.none),
                  )
                ],
              ),
              onTap: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => ProfileScreen()),
                );
                // Update the state of the app.
                // ...
              },
            ),
          ],
        )),
        appBar: CustomAppBar(
          height: MediaQuery.of(context).size.height * 15 / 100,
        ),
        backgroundColor: Colors.white,
        body: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(children: [
            Container(
                height: MediaQuery.of(context).size.height * 0.70,
                width: MediaQuery.of(context).size.width,
                child: Theme(
                    data: ThemeData(
                        accentColor: Color.fromRGBO(63, 137, 166, 1),
                        colorScheme: ColorScheme.light(
                            primary: Color.fromRGBO(63, 137, 166, 1))),
                    child: _cupertinoStepper(StepperType.vertical))),

            Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 13 / 100,
                decoration: BoxDecoration(
                    border: Border(
                        top: BorderSide(color: Colors.grey[500], width: 0.5))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.fromLTRB(
                          MediaQuery.of(context).size.width * 3 / 100,
                          0,
                          MediaQuery.of(context).size.width * 3 / 100,
                          0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Total Amount",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18),
                          ),
                          Text("0.00\$",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18))
                        ],
                      ),
                    ),
                    Container(
                      height: 20,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 90 / 100,
                      height: 40,
                      child: MaterialButton(
                        color: Color(0xff3F89A6),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(
                                color: Color(0xff3F89A6),
                                width: 1,
                                style: BorderStyle.solid),
                            borderRadius: BorderRadius.circular(50)),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => Signup3(),
                          ));
                        },
                        child: Text("DONE",
                            style: TextStyle(
                                fontFamily: "ink free",
                                fontSize:
                                    MediaQuery.of(context).size.width * 0.06,
                                fontWeight: FontWeight.w300,
                                color: Colors.white,
                                decoration: TextDecoration.none)),
                      ),
                    )
                  ],
                )),
          ]),
        ));
  }

  CupertinoStepper _cupertinoStepper(StepperType type) {
    final canCancel = _currentStep > 0;
    final canContinue = _currentStep < 4;
    return CupertinoStepper(
        type: type,
        currentStep: _currentStep,
        onStepTapped: (step) => setState(() => _currentStep = step),
        onStepContinue: canContinue
            ? () => setState(() {
                  for (int i = 0;
                      i < customListView[_currentStep].cardData.length;
                      i++) {
                    print(customListView[_currentStep].cardData[i].pressed);

                    if (customListView[_currentStep].cardData[i].pressed ==
                        true) {
                      return ++_currentStep;
                    }
                  }
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Container(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: Icon(
                            FeatherIcons.info,
                            color: Colors.white,
                          ),
                        ),
                        Text('You must choose one item'),
                      ],
                    )),
                    backgroundColor: Colors.red,
                    duration: Duration(seconds: 1),
                  ));
                })
            : null,
        onStepCancel: canCancel ? () => setState(() => --_currentStep) : null,
        steps: [
          for (int i = 0; i < ListviewData.length; i++)
            Step(
              title: Text(ListviewData[i]["title"]),
              isActive: i == _currentStep,
              state: i == _currentStep
                  ? StepState.editing
                  : i < _currentStep
                      ? StepState.complete
                      : StepState.indexed,
              content: Container(
                height: MediaQuery.of(context).size.height * 0.50,
                width: MediaQuery.of(context).size.width,
                child: ListView.builder(
                    physics: BouncingScrollPhysics(),
                    itemCount: 1,
                    itemBuilder: (BuildContext ctxt, int index) {
                      return customListView[i];
                    }),
              ),
            )
        ]);
  }
}
