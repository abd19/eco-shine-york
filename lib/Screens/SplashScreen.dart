import 'dart:async';

import 'package:fluttercatalog/Login/signin.dart';

import '../utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'WelcomeScreen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new Stack(
        fit: StackFit.expand,
        alignment: Alignment.bottomCenter,
        children: [
          Container(
            decoration: BoxDecoration(color: Colors.white),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center ,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              new Expanded(
                child: new Image.asset(
                  'assets/images/Logo.png',
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  fit: BoxFit.contain,
                  alignment: Alignment.topCenter,
                ),
              ),
              new Expanded(
                child: Container(
                  child: new Image.asset(
                    'assets/images/bottom.png',
                    height: MediaQuery.of(context).size.height ,
                    width: MediaQuery.of(context).size.width ,
                    fit: BoxFit.contain ,
                    alignment: Alignment.bottomCenter,
                  ),
                  transform: Matrix4.translationValues(
                      MediaQuery.of(context).size.width * .0, 60.0, 0.0),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 5), () => {checkFirstSeen()});
  }

  Future checkFirstSeen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _seen = (prefs.getBool('seen') ?? false);

    if (_seen) {
      Navigator.of(context).pushReplacement(
          new MaterialPageRoute(builder: (context) => new SignIn()));
    } else {
      await prefs.setBool('seen', true);
      Navigator.of(context).pushReplacement(
          new MaterialPageRoute(builder: (context) => new WelcomeScreen()));
    }
  }
}
