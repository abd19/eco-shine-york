import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:fluttercatalog/Home.dart';
import 'package:fluttercatalog/utils/styles.dart';
import 'package:intl/intl.dart';

class ProfileScreen extends StatelessWidget {
  final dateFromat = new DateFormat('yyyy-MM-dd hh:mm');

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Stack(
      fit: StackFit.expand,
      children: [
        Container(

          decoration: BoxDecoration(

            color: Colors.white,
            image: DecorationImage(
              image: AssetImage("assets/images/ProfileBG.png"),
              fit: BoxFit.fitWidth,alignment: Alignment.topCenter

            ),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          body: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 73),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      IconButton(
                        icon: Icon(                        FeatherIcons.arrowLeft,
                            color: Colors.white
                        ),
                       onPressed: (){
                         Navigator.pushReplacement(
                           context,
                           MaterialPageRoute(builder: (context) => Home()),
                         );



                       },
                      ),

                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'My Profile',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 34,
                      fontFamily: 'ink free',
                      fontWeight: FontWeight.bold
                    ),
                  ),
                  SizedBox(
                    height: 22,
                  ),
                  Container(
                    height: height * 0.43,
                    child: LayoutBuilder(
                      builder: (context, constraints) {
                        double innerHeight = constraints.maxHeight;
                        double innerWidth = constraints.maxWidth;
                        return Stack(
                          fit: StackFit.expand,
                          children: [
                            Positioned(
                              bottom: 0,
                              left: 0,
                              right: 0,
                              child: Container(
                                height: innerHeight * 0.72,
                                width: innerWidth,
                                decoration: BoxDecoration(
                                  border:  Border.all(color: CustomColors.primaryColor
                                      ,width: 2),
                                  borderRadius: BorderRadius.circular(30),
                                  color: Colors.white,
                                ),
                                child: Column(
                                  children: [
                                    SizedBox(
                                      height: 80,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top:4.0),
                                      child: Text(
                                        'Abd olabi',
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontFamily: 'ink free',
                                          fontSize: 37,
                                          fontWeight: FontWeight.w700
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Column(
                                          children: [
                                            Text(
                                              'Cars',
                                              style: TextStyle(
                                                color: CustomColors.primaryColor,
                                                fontFamily: 'ink free',
                                                fontSize: 25,
                                                fontWeight: FontWeight.bold
                                              ),
                                            ),
                                            Text(
                                              '10',
                                              style: TextStyle(
                                                color: Colors.black,
                                                fontFamily: 'ink free',
                                                fontSize: 25,
                                              ),
                                            ),
                                          ],
                                        ),


                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Positioned(
                              top: 0,
                              left: 0,
                              right: 0,
                              child: Center(
                                child: Container(
                                  child: Image.asset(
                                    'assets/images/Cars.png',
                                    width: innerWidth * 0.45,
                                    fit: BoxFit.fitWidth,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                    height: height * 0.6,
                    width: width,
                    decoration: BoxDecoration(     border:  Border.all(color: CustomColors.primaryColor
                        ,width: 2),
                      borderRadius: BorderRadius.circular(30),
                      color: Colors.white,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: Column(
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            'My Washing',
                            style: TextStyle(
                              color:CustomColors.primaryColor,
                              fontSize: 27,
                              fontFamily: 'ink free',
                              fontWeight: FontWeight.bold
                            ),
                          ),
                          Divider(
                            thickness: 2.5,
                          ),

                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: height*0.20,
                            decoration: BoxDecoration(
                              color: CustomColors.secondaryColor,
                              borderRadius: BorderRadius.circular(30),

                            ),
                            child: Padding(
                              padding: const EdgeInsets.only(right:4.0,left: 4.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Car name',
                                        style: TextStyle(
                                            color:Colors.white,
                                            fontSize:25,
                                            fontFamily: 'ink free',
                                            fontWeight: FontWeight.w700
                                        ),
                                      ),
                                      Text(
                                        dateFromat.format(new DateTime.now()),
                                        style: TextStyle(
                                            color:Colors.white,
                                            fontSize: 20,
                                            fontFamily: 'ink free',
                                            fontWeight: FontWeight.w300
                                        ),
                                      ),

                                    ],


                                  )
                                  ,Text(
                                    '23\$',
                                    style: TextStyle(
                                        color:Colors.white,
                                        fontSize:25,
                                        fontFamily: 'ink free',
                                        fontWeight: FontWeight.w700
                                    ),
                                  ),
                                ],


                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: height*0.20,
                            decoration: BoxDecoration(
                              color: CustomColors.secondaryColor,
                              borderRadius: BorderRadius.circular(30),

                            ),
                            child: Padding(
                              padding: const EdgeInsets.only(right:4.0,left: 4.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Car name',
                                        style: TextStyle(
                                            color:Colors.white,
                                            fontSize:25,
                                            fontFamily: 'ink free',
                                            fontWeight: FontWeight.w700
                                        ),
                                      ),
                                      Text(
                                        dateFromat.format(new DateTime.now()),
                                        style: TextStyle(
                                            color:Colors.white,
                                            fontSize: 20,
                                            fontFamily: 'ink free',
                                            fontWeight: FontWeight.w300
                                        ),
                                      ),

                                    ],


                                  )
                                  ,Text(
                                    '23\$',
                                    style: TextStyle(
                                        color:Colors.white,
                                        fontSize:25,
                                        fontFamily: 'ink free',
                                        fontWeight: FontWeight.w700
                                    ),
                                  ),
                                ],


                              ),
                            ),
                          )

                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        )
      ],
    );
  }










}



