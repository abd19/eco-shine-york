import 'package:fluttercatalog/Login/signin.dart';

import '../utils/styles.dart';
import 'package:flutter/material.dart';


class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  int currentPage = 0;
  List<Map<String, String>> contentData = [
    {
      "text": "SELECT WHAT YOU\nWANT TO CLEAN ",
      "image": "assets/images/Cars.png",
      "image2": "assets/images/home.png",
      "screen": "true"
    },
    {
      "text":
          "JUST POINT ON THE\n MAP WHERE YOUR CAR\nIS AND WE WILL DO \n THE REST  ",
      "image": "assets/images/place.png",
      "image2": "",
      "screen": "false"
    },
    {
      "text": "FROM A VARIATION \nOF SERVICE' TYPES\nSELECT ONE",
      "image": "assets/images/package.png",
      "image2": "",
      "screen": "false"
    },
    {
      "text": "WE OFFER YOU MANY\nMORE EXTRAS WHICH\nYOU CAN CHOOSE\nFROM ",
      "image": "assets/images/choose.png",
      "image2": "",
      "screen": "false"
    },
    {
      "text": "FINALLY, JUST\nCHOOSE THE DATE\nAND TIME AND WE\nWILL BE THERE",
      "image": "assets/images/hours.png",
      "image2": "",
      "screen": "false"
    },
  ];
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Stack(
          children: [
            Container(
              child: PageView.builder(
                  controller: pageController,
                  onPageChanged: (value) {
                    setState(() {
                      currentPage = value;
                    });
                  },
                  itemCount: contentData.length,
                  itemBuilder: (context, index) => WelcomeScreenContent(
                        text: contentData[index]["text"],
                        image: contentData[index]["image"],
                        image2: contentData[index]["image2"],
                        Screen: contentData[index]["screen"] == "true"
                            ? true
                            : false,
                      )),
            ),
            Padding(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).size.height * 0.02,
                    left: MediaQuery.of(context).size.height * 0.01),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Row(
                      children: List.generate(
                          contentData.length, (index) => bulidDots(index)),
                    )
                  ],
                )),
            Align(
                alignment: Alignment.bottomRight,
                child: TextButton(
                  style: TextButton.styleFrom(
                    primary: Colors.white,
                    shadowColor: Colors.black,
                    elevation: 20,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(100)),
                  ),
                  onPressed: () {
                    if (currentPage >= 0 && currentPage < 4)
                      pageController.animateToPage(
                        4,
                        duration: Duration(milliseconds: 300),
                        curve: Curves.linear,
                      );
                    else
                      Navigator.of(context).pushReplacement(
                          new MaterialPageRoute(
                              builder: (context) => new SignIn()));
                  },
                  child: Text(
                      (currentPage >= 0 && currentPage < 4) ? "Skip" : "Done",
                      style: TextStyle(

                        fontFamily: "ink free",
                        fontSize: MediaQuery.of(context).size.width * 0.06,
                        fontWeight: FontWeight.w100,
                        color: Colors.white,
                        decoration: TextDecoration.none,
                      )),
                ))
          ],
        ),
      ),
    );
  }

  Widget bulidDots(int index) {
    return Container(
      margin: EdgeInsets.only(right: 4),
      height: 15,
      width: 15,
      decoration: BoxDecoration(
          color: currentPage == index ? Color(0xff3F89A6) : Color(0xffD0E9F2),
          borderRadius: BorderRadius.circular(10)),
    );
  }
}

class WelcomeScreenContent extends StatelessWidget {
  const WelcomeScreenContent(
      {Key key, this.image, this.text, this.Screen, this.image2})
      : super(key: key);
  final String image, image2, text;
  final bool Screen;

  @override
  Widget build(BuildContext context) {
    //mobile view
    if (MediaQuery.of(context).size.height > MediaQuery.of(context).size.width)
      return Scaffold(
          body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            child: new Image.asset(
              'assets/images/top.png',
              height: MediaQuery.of(context).size.height * 0.35,
              width: MediaQuery.of(context).size.width + 100,
              fit: BoxFit.fill,
              alignment: Alignment.topRight,
            ),
          ),
          Padding(
            padding:
                EdgeInsets.only(top: MediaQuery.of(context).size.width * 0.10),
            child: new Text(
              text,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontFamily: "ink free",
                  fontSize: MediaQuery.of(context).size.width * 0.07,
                  fontWeight: FontWeight.w100,
                  color: Colors.white,
                  decoration: TextDecoration.none),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.width * 0.40,
                right: MediaQuery.of(context).size.width * 0.40),
            child: new Image.asset(
              image,
              height: MediaQuery.of(context).size.height - 250,
              width: MediaQuery.of(context).size.width - 250,
              fit: BoxFit.contain,
              alignment: Alignment.centerLeft,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.width * 0.25,
                left: MediaQuery.of(context).size.width * 0.55),
            child: Visibility(
              visible: Screen,
              child: new Image.asset(
                image2,
                height: MediaQuery.of(context).size.height - 250,
                width: MediaQuery.of(context).size.width - 250,
                fit: BoxFit.contain,
                alignment: Alignment.centerRight,
              ),
            ),
          ),
          Align(
              alignment: Alignment.bottomCenter,
              child: Stack(alignment: Alignment.bottomCenter, children: [
                Container(
                  child: new Image.asset(
                    'assets/images/bottomright.png',
                    fit: BoxFit.contain,
                    alignment: Alignment.bottomRight,
                  ),
                  transform: Matrix4.translationValues(
                      MediaQuery.of(context).size.width * .30, 180.0, 0.0),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).size.width * 0.10,
                      left: MediaQuery.of(context).size.width * 0.42),
                  child: new Image.asset(
                    'assets/images/man.png',
                    fit: BoxFit.fill,
                    height: MediaQuery.of(context).size.height * 0.32,
                    width: MediaQuery.of(context).size.width * 0.80,
                    alignment: Alignment.bottomRight,
                  ),
                ),
              ]))
        ],
      ));
    //tablet view
    else
      return Scaffold(
          body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            child: new Image.asset(
              'assets/images/top.png',
              height: MediaQuery.of(context).size.height*0.45,
              width: MediaQuery.of(context).size.width ,
              fit: BoxFit.fill,
              alignment: Alignment.topRight,
            ),
          ),
          Padding(
            padding:
                EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
            child: new Text(
              text,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontFamily: "ink free",
                  fontSize: MediaQuery.of(context).size.width * 0.06,
                  fontWeight: FontWeight.w100,
                  color: Colors.white,
                  decoration: TextDecoration.none),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.width * 0.38,
                right: MediaQuery.of(context).size.width * 0.30),
            child: new Image.asset(
              image,
              height: MediaQuery.of(context).size.height - 500,
              width: MediaQuery.of(context).size.width - 500,
              fit: BoxFit.contain,
              alignment: Alignment.centerLeft,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.width * 0.55,
                left: MediaQuery.of(context).size.width * 0.25),
            child: Visibility(
              visible: Screen,
              child: new Image.asset(
                image2,
                height: MediaQuery.of(context).size.height - 250,
                width: MediaQuery.of(context).size.width - 250,
                fit: BoxFit.contain,
                alignment: Alignment.centerRight,
              ),
            ),
          ),
          Align(
              alignment: Alignment.bottomCenter,
              child: Stack(alignment: Alignment.bottomCenter, children: [
                Container(
                  child: new Image.asset(
                    'assets/images/bottomright.png',
                    fit: BoxFit.contain,
                    alignment: Alignment.bottomRight,
                  ),
                  transform: Matrix4.translationValues(
                      MediaQuery.of(context).size.width * .36, 430.0, 0.0),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    bottom:  MediaQuery.of(context).size.width * 0.04,
                      left: MediaQuery.of(context).size.width * 0.55),
                  child: new Image.asset(
                    'assets/images/man.png',
                    fit: BoxFit.contain,
                    height: MediaQuery.of(context).size.height * 0.75,
                    width: MediaQuery.of(context).size.width * 0.30,
                    alignment: Alignment.bottomRight ,
                  ),
                ),
              ]))
        ],
      ));
  }
}
