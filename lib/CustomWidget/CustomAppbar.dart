import 'package:flutter/material.dart';

import '../Screens/StoryScreen.dart';

class CustomAppBar extends PreferredSize {
  final double height;

  CustomAppBar({ this.height = kToolbarHeight});

  @override
  Size get preferredSize => Size.fromHeight(height);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height*15/100,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        // color: Colors.red,
        image: DecorationImage(alignment: Alignment.topCenter,
          image: AssetImage("assets/images/appbar.png"),
          fit: BoxFit.fill,
        ),
      ),
      child: Stack(alignment:Alignment.center,children: [
// left: MediaQuery.of(context).size.width * 0.03,
//            top: MediaQuery.of(context).size.height * 0.04,
//

        Padding(
          padding:  EdgeInsets.only(top:MediaQuery.of(context).size.height*0.01),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,

            children: [
              IconButton(icon: Icon(Icons.menu), onPressed: (){
                Scaffold.of(context).openDrawer();
              },alignment: Alignment.center,color: Colors.white,),
              Spacer(),
              IconButton(

                icon: Icon(
                  Icons.play_circle_outline,
                  color:Colors.white,

                ),
                onPressed: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) => StoryScreen())),
                alignment: Alignment.centerLeft,
              ),
            ],
          ),
        )
,


      ]),
    );
  }
}
