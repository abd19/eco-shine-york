import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomCard extends StatefulWidget {
  CustomCard({Key key, this.text, this.price, this.icon, this.info,this.pressed})
      : super(key: key);
  String  price, text;
  Widget info;
  bool pressed = false;
  Widget icon;

  @override
  _CustomCardState createState() {
    _CustomCardState state = new _CustomCardState(icon, price, text, info, pressed);

    return state;
  }
}

class _CustomCardState extends State<CustomCard> {
  String price, text;
  Widget info;
  Widget icon;
  bool pressed= false;
  _CustomCardState(this.icon, this.price, this.text, this.info, this.pressed);

  @override
  Widget build(BuildContext context) {
    return  Container(
        height: MediaQuery.of(context).size.height * 0.17,
        width: MediaQuery.of(context).size.width * 0.40,
        decoration: BoxDecoration(

            // border: Border.all(
            //   color: pressed? Color(0xff3F89A6):Colors.grey,
            //   width: pressed? 3:1,
            // ),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.04),
                spreadRadius: 5,
                blurRadius: 7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
            color: Colors.white,
            borderRadius: BorderRadius.circular(20)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.02,
                      top: MediaQuery.of(context).size.width * 0.0),
                  child: icon
                ),
                new Spacer(), // I just added one line

                info
              ],
            ),
            Padding(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.028,
                  left: MediaQuery.of(context).size.height * 0.01),
              child: Align(
                alignment: Alignment.bottomLeft,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        text,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: "ink free",
                            fontSize: MediaQuery.of(context).size.width * 0.05,
                            fontWeight: FontWeight.w100,
                            color: Colors.black,
                            decoration: TextDecoration.none),
                      ),
                      Text(
                        price,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: "ink free",
                            fontSize: MediaQuery.of(context).size.width * 0.03,
                            fontWeight: FontWeight.w100,
                            color: Colors.black,
                            decoration: TextDecoration.none),
                      )
                    ]),
              ),
            ),
          ],
        ),

    );
  }
}
