import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:fluttercatalog/utils/styles.dart';

import 'CustomCard.dart';

class CustomListView extends StatefulWidget {
   _CustomListViewState state ;

  @override
  _CustomListViewState createState() {
    state = new _CustomListViewState();

    return state;
  }

   List<CustomCard> get cardData => state._cardData;
}

class _CustomListViewState extends State<CustomListView> {
  List<CustomCard> _cardData = [
    CustomCard(
        icon: Container(
          width: 32,
          height: 32,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50.0),
            color: CustomColors.secondaryColor,
          ),
          child: Center(
            child: Icon(
              FeatherIcons.alertCircle,
              color: Colors.white,
            ),
          ),
        ),
        text: "hello2",
        price: "25.0",
        pressed: false,
        info: IconButton(
          icon: Icon(Icons.camera_alt),
          onPressed: () {
            print("fuck abd");
          },
        )),
    CustomCard(
        icon: Container(
          width: 32,
          height: 32,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50.0),
            color: CustomColors.secondaryColor,
          ),
          child: Center(
            child: Icon(
              FeatherIcons.alertCircle,
              color: Colors.white,
            ),
          ),
        ),
        text: "hello2",
        price: "25.0",
        pressed: false,
        info: IconButton(
          icon: Icon(Icons.camera_alt),
          onPressed: () {
            print("fuck abd");
          },
        )),
    // CustomCard(
    //   icon: Container(
    //     width: 32,
    //     height: 32,
    //     decoration: BoxDecoration(
    //       borderRadius: BorderRadius.circular(50.0),
    //       color: CustomColors.secondaryColor,
    //     ),
    //     child: Center(
    //       child: Icon(FeatherIcons.alertCircle,color: Colors.white,),
    //     ),
    //   ),
    //   text: "hello2",
    //   price: "25.0",
    //   info: "bla bla bla ",
    // ),
    // CustomCard(
    //   icon: Container(
    //     width: 32,
    //     height: 32,
    //     decoration: BoxDecoration(
    //       borderRadius: BorderRadius.circular(50.0),
    //       color: CustomColors.secondaryColor,
    //     ),
    //     child: Center(
    //       child: Icon(FeatherIcons.alertCircle,color: Colors.white,),
    //     ),
    //   ),
    //   text: "hello2",
    //   price: "25.0",
    //   info: "bla bla bla ",
    // ),
    // CustomCard(
    //   icon: Container(
    //     width: 32,
    //     height: 32,
    //     decoration: BoxDecoration(
    //       borderRadius: BorderRadius.circular(50.0),
    //       color: CustomColors.secondaryColor,
    //     ),
    //     child: Center(
    //       child: Icon(FeatherIcons.alertCircle,color: Colors.white,),
    //     ),
    //   ),
    //   text: "hello2",
    //   price: "25.0",
    //   info: "bla bla bla ",
    // ),
    // CustomCard(
    //   icon: Container(
    //     width: 32,
    //     height: 32,
    //     decoration: BoxDecoration(
    //       borderRadius: BorderRadius.circular(50.0),
    //       color: CustomColors.secondaryColor,
    //     ),
    //     child: Center(
    //       child: Icon(FeatherIcons.alertCircle,color: Colors.white,),
    //     ),
    //   ),
    //   text: "hello2",
    //   price: "25.0",
    //   info: "bla bla bla ",
    // ),
    // CustomCard(
    //   icon: Container(
    //     width: 32,
    //     height: 32,
    //     decoration: BoxDecoration(
    //       borderRadius: BorderRadius.circular(50.0),
    //       color: CustomColors.secondaryColor,
    //     ),
    //     child: Center(
    //       child: Icon(FeatherIcons.alertCircle,color: Colors.white,),
    //     ),
    //   ),
    //   text: "hello2",
    //   price: "25.0",
    //   info: "bla bla bla ",
    // ),
  ];

  List<CustomCard> get cardData => _cardData;

  set cardData(List<CustomCard> value) {
    _cardData = value;
  }

  int counter = 0;
  bool show = true;
  ScrollController controller = new ScrollController();

  @override
  void initState() {
    // TODO: implement initState
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height * 0.40,
        width: MediaQuery.of(context).size.width,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(

                child: GridView.builder(
                  controller: controller,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      mainAxisSpacing:
                          MediaQuery.of(context).size.height * 0.08,
                      crossAxisSpacing:
                          MediaQuery.of(context).size.width * 0.02),
                  itemBuilder: (_, index) => Container(

                    decoration: BoxDecoration(
                        border: Border.all(
                          color: _cardData[index].pressed
                              ? Color(0xff3F89A6)
                              : Colors.grey,
                          width: _cardData[index].pressed ? 3 : 1,
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.04),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20)),
                    child: GestureDetector(
                      onTap: () {
                        if (_cardData[index].pressed == false) {
                          for (int i = 0; i < _cardData.length; i++) {
                            if (_cardData[i].pressed == true) {
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(SnackBar(
                                content: Container(
                                    child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(right: 8.0),
                                      child: Icon(
                                        FeatherIcons.info,
                                        color: Colors.white,
                                      ),
                                    ),
                                    Text('You can\'t choose more than one'),
                                  ],
                                )),
                                backgroundColor: Colors.red,
                                duration: Duration(seconds: 1),
                              ));

                              return;
                            }
                          }
                          setState(() {
                            _cardData[index].pressed = true;
                            show = false;
                          });
                        } else
                          setState(() {
                            _cardData[index].pressed = false;
                          });
                      },
                      child: CustomCard(
                        info: _cardData[index].info,
                        icon: _cardData[index].icon,
                        text: _cardData[index].text,
                        price: _cardData[index].price,
                        pressed: _cardData[index].pressed,
                      ),
                    ),
                  ),
                  itemCount: _cardData != null ? _cardData.length : 0,
                ),
              ),
            ]));
  }

  TextStyle headerStyle() {
    return TextStyle(
        fontFamily: "ink free",
        fontSize: MediaQuery.of(context).size.width * 0.06,
        fontWeight: FontWeight.w300,
        color: Colors.black,
        decoration: TextDecoration.none);
  }
}
