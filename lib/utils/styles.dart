import 'package:flutter/material.dart';

/// Colors ///
class CustomColors {
  static Color primaryColor = Color.fromRGBO(63, 137, 166, 1);
  static Color secondaryColor = Color.fromRGBO(150, 198, 217, 1);
  static Color secondaryColor2 = Color.fromRGBO(208, 233, 242, 1);

  // Logins Colors
  static Color lineColor = Colors.grey[700];

}

/// Text Styles ///
class CustomTextStyles {
  // Logins Styles
  static TextStyle normalLogin = TextStyle(fontFamily: 'InkFree', fontSize: 14.0);
  static TextStyle textButton = TextStyle(fontFamily: 'InkFree', fontSize: 19.0, color: CustomColors.primaryColor);
}
