import '../utils/styles.dart';
import 'package:flutter/material.dart';

import 'signup3.dart';

class Signup2 extends StatefulWidget {
  @override
  _Signup2State createState() => _Signup2State();
}

class _Signup2State extends State<Signup2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: [
            new Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 70/100,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/signinBG.png'),
                      fit: BoxFit.fill
                  )
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  new Image.asset('assets/images/loginLogo.png',width: 240),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      new Container(
                        height: 1.0,
                        width: MediaQuery.of(context).size.width * 9/100,
                        color: CustomColors.lineColor,
                      ),
                      new Container(
                        width: MediaQuery.of(context).size.width * 70/100,
                        child: new Text('Please check your email, then enter the verification code we sent',style: CustomTextStyles.normalLogin,textAlign: TextAlign.center),
                      ),
                      new Container(
                        height: 1.0,
                        width: MediaQuery.of(context).size.width * 9/100,
                        color: CustomColors.lineColor,
                      )
                    ],
                  ),
                  new Form(
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 60.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 75/100,
                            height: MediaQuery.of(context).size.height * 5.8/100,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(20.0)
                            ),
                            child: new TextFormField(
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  fillColor: Colors.white,
                                  hintText: 'Verification code',
                                  contentPadding: EdgeInsets.only(left: 10.0),
                                  hintStyle: CustomTextStyles.normalLogin
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  new Text("Didn't get any email?",style: CustomTextStyles.normalLogin),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      new Container(
                        height: 1.0,
                        width: 50.0,
                        color: CustomColors.lineColor,
                      ),
                      new Container(
                        height: 1.0,
                        width: 160.0,
                        color: Colors.transparent,
                      ),
                      new Container(
                        height: 1.0,
                        width: 50.0,
                        color: CustomColors.lineColor,
                      ),
                    ],
                  ),
                  new GestureDetector(
                    onTap: (){},
                    child: Text('Resend email',style: CustomTextStyles.textButton),
                  )
                ],
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_forward_rounded),
        onPressed: (){
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Signup3()),
          );
        },
        backgroundColor: CustomColors.primaryColor,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}

