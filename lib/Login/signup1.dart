
import '../utils/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'signin.dart';
import 'signup2.dart';

class Signup1 extends StatefulWidget {
  @override
  _Signup1State createState() => _Signup1State();
}

class _Signup1State extends State<Signup1> {
  final _formkey = GlobalKey<FormState>();
  final TextEditingController _passWord = TextEditingController();
  final TextEditingController _confirmPassword = TextEditingController();

  bool terms = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: [
            new Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 85 / 100,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/signinBG.png'),
                      fit: BoxFit.fill)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  new Image.asset('assets/images/loginLogo.png', width: 240),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      new Container(
                        height: 1.0,
                        width: MediaQuery.of(context).size.width * 5 / 100,
                        color: CustomColors.lineColor,
                      ),
                      new Text('Sign up', style: CustomTextStyles.normalLogin),
                      new Container(
                        height: 1.0,
                        width: MediaQuery.of(context).size.width * 60 / 100,
                        color: CustomColors.lineColor,
                      )
                    ],
                  ),
                  new Form(
                    key: _formkey,
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 30.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 75 / 100,
                            height:
                                MediaQuery.of(context).size.height * 5.8 / 100,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(20.0)),
                            child: new TextFormField(
                              validator: (test) {
                                if (test.isEmpty) return "Please enter Email";
                                if (test.contains("@"))
                                  return "Please enter valid Email";
                              },
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  fillColor: Colors.white,
                                  hintText: 'Email',
                                  contentPadding: EdgeInsets.only(left: 10.0),
                                  hintStyle: CustomTextStyles.normalLogin),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 30.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 75 / 100,
                            height:
                                MediaQuery.of(context).size.height * 5.8 / 100,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(20.0)),
                            child: new TextFormField(
                              controller: _passWord,
                              validator: (test) {
                                if (test.isEmpty) return "Please enter Password";
                              },
                              obscureText: true,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  fillColor: Colors.white,
                                  hintText: 'Password',
                                  contentPadding: EdgeInsets.only(left: 10.0),
                                  hintStyle: CustomTextStyles.normalLogin),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 75 / 100,
                            height:
                                MediaQuery.of(context).size.height * 5.8 / 100,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(20.0)),
                            child: new TextFormField(
                              controller: _confirmPassword,
                              validator: (test) {
                                if (test.isEmpty)
                                  return "Please enter Password";
                                else if (test != _passWord.text)
                                  return "Password not match !";
                              },
                              obscureText: true,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  fillColor: Colors.white,
                                  hintText: 'Confirm password',
                                  contentPadding: EdgeInsets.only(left: 10.0),
                                  hintStyle: CustomTextStyles.normalLogin),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.only(bottom: 30.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        new Checkbox(
                          onChanged: (bool newValue) {
                            setState(() {
                              terms = newValue;
                            });
                          },
                          activeColor: CustomColors.primaryColor,
                          value: terms,
                        ),
                        new Text("Agree with our",
                            style: CustomTextStyles.normalLogin),
                        new GestureDetector(
                            onTap: () {},
                            child: Text(' Terms',
                                style: CustomTextStyles.textButton))
                      ],
                    ),
                  ),
                  new Text('Have an account?',
                      style: CustomTextStyles.normalLogin),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      new Container(
                        height: 1.0,
                        width: 50.0,
                        color: CustomColors.lineColor,
                      ),
                      new Container(
                        height: 1.0,
                        width: 140.0,
                        color: Colors.transparent,
                      ),
                      new Container(
                        height: 1.0,
                        width: 50.0,
                        color: CustomColors.lineColor,
                      ),
                    ],
                  ),
                  new GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => SignIn()),
                      );
                    },
                    child: Text('Sign in', style: CustomTextStyles.textButton),
                  )
                ],
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_forward_rounded),
        onPressed: () {
          if (_formkey.currentState.validate())
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Signup2()),
            );
        },
        backgroundColor: CustomColors.primaryColor,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
