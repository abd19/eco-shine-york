import '../utils/styles.dart';
import 'package:flutter/material.dart';
import '../Home.dart';

class Signup3 extends StatefulWidget {
  @override
  _Signup3State createState() => _Signup3State();
}

class _Signup3State extends State<Signup3> {
  final _formkey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: [
            new Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 75/100,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/signinBG.png'),
                      fit: BoxFit.fill
                  )
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  new Image.asset('assets/images/loginLogo.png',width: 240),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      new Container(
                        height: 1.0,
                        width: MediaQuery.of(context).size.width * 5/100,
                        color: CustomColors.lineColor,
                      ),
                      new Text('Personal information',style: CustomTextStyles.normalLogin),
                      new Container(
                        height: 1.0,
                        width: MediaQuery.of(context).size.width * 35/100,
                        color: CustomColors.lineColor,
                      )
                    ],
                  ),
                  new Form(
                    key: _formkey,

                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 30.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 75/100,
                            height: MediaQuery.of(context).size.height * 5.8/100,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(20.0)
                            ),
                            child: new TextFormField(
                              validator: (test) {
                                if (test.isEmpty) return "Please enter name";
                              },
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  fillColor: Colors.white,
                                  hintText: 'Full name',
                                  contentPadding: EdgeInsets.only(left: 10.0),
                                  hintStyle: CustomTextStyles.normalLogin
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 30.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 75/100,
                            height: MediaQuery.of(context).size.height * 5.8/100,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(20.0)
                            ),
                            child: new TextFormField(
                              validator: (test) {
                                if (test.isEmpty) return "Please enter Car licence";
                              },
                              obscureText: true,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  fillColor: Colors.white,
                                  hintText: 'Car licence',
                                  contentPadding: EdgeInsets.only(left: 10.0),
                                  hintStyle: CustomTextStyles.normalLogin
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 30.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 75/100,
                            height: MediaQuery.of(context).size.height * 5.8/100,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(20.0)
                            ),
                            child: new TextFormField(
                              validator: (test) {
                                if (test.isEmpty) return "Please enter Phone number";
                              },
                              obscureText: true,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  fillColor: Colors.white,
                                  hintText: 'Phone number',
                                  contentPadding: EdgeInsets.only(left: 10.0),
                                  hintStyle: CustomTextStyles.normalLogin
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_forward_rounded),
        onPressed: (){


          if (_formkey.currentState.validate()) {
            Navigator.of(context).pushReplacement(
                new MaterialPageRoute(
                    builder: (context) => new Home()));
          }
        },
        backgroundColor: CustomColors.primaryColor,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
