import 'package:flutter/material.dart';
import 'package:fluttercatalog/utils/styles.dart';

import '../Home.dart';
import 'signup1.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final _formkey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: [
            new Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 70 / 100,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/signinBG.png'),
                      fit: BoxFit.fill)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  new Image.asset('assets/images/loginLogo.png', width: 240),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      new Container(
                        height: 1.0,
                        width: MediaQuery.of(context).size.width * 5 / 100,
                        color: CustomColors.lineColor,
                      ),
                      new Text('Sign in', style: CustomTextStyles.normalLogin),
                      new Container(
                        height: 1.0,
                        width: MediaQuery.of(context).size.width * 60 / 100,
                        color: CustomColors.lineColor,
                      )
                    ],
                  ),
                  new Form(
                    key: _formkey,
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 30.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 75 / 100,
                            height:
                                MediaQuery.of(context).size.height * 5.8 / 100,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(20.0)),
                            child: new TextFormField(
                              validator: (test) {
                                if (test.isEmpty) return "Please enter Email";
                                if (test.contains("@"))
                                  return "Please enter valid Email";
                              },
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  fillColor: Colors.white,
                                  hintText: 'Email',
                                  contentPadding: EdgeInsets.only(left: 10.0),
                                  hintStyle: CustomTextStyles.normalLogin),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 50.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 75 / 100,
                            height:
                                MediaQuery.of(context).size.height * 5.8 / 100,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(20.0)),
                            child: new TextFormField(
                              validator: (test) {
                                if (test.isEmpty)
                                  return "Please enter Password";
                              },
                              obscureText: true,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  fillColor: Colors.white,
                                  hintText: 'Password',
                                  contentPadding: EdgeInsets.only(left: 10.0),
                                  hintStyle: CustomTextStyles.normalLogin),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  new Text('First timer?', style: CustomTextStyles.normalLogin),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      new Container(
                        height: 1.0,
                        width: 50.0,
                        color: CustomColors.lineColor,
                      ),
                      new Container(
                        height: 1.0,
                        width: 100.0,
                        color: Colors.transparent,
                      ),
                      new Container(
                        height: 1.0,
                        width: 50.0,
                        color: CustomColors.lineColor,
                      ),
                    ],
                  ),
                  new GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Signup1()),
                      );
                    },
                    child: Text('Sign up', style: CustomTextStyles.textButton),
                  )
                ],
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_forward_rounded),
        onPressed: () {
          if (_formkey.currentState.validate()) {
            Navigator.of(context).pushReplacement(
                new MaterialPageRoute(builder: (context) => new Home()));
          }
        },
        backgroundColor: CustomColors.primaryColor,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
