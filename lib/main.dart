import 'package:flutter/material.dart';

import 'Screens/SplashScreen.dart';


void main() {
  runApp(new MaterialApp(
    debugShowCheckedModeBanner: false,
    home: new SplashScreen(),
  ));
}
